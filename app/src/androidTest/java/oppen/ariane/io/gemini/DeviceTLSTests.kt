package oppen.ariane.io.gemini

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.security.SecureRandom
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocket
import javax.net.ssl.SSLSocketFactory

@RunWith(AndroidJUnit4::class)
class DeviceTLSTests {

    lateinit var socket: SSLSocket

    @Before
    fun setupSocket(){
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, null, SecureRandom())
        val factory: SSLSocketFactory = sslContext.socketFactory
        socket = factory.createSocket() as SSLSocket
    }

    @Test
    fun supportsTLSv1(){
        socket.supportedProtocols.contains("TLSv1")
    }

    @Test
    fun supportsTLSv1_1(){
        socket.supportedProtocols.contains("TLSv1.1")
    }

    @Test
    fun supportsTLSv1_2(){
        socket.supportedProtocols.contains("TLSv1.2")
    }

    @Test
    fun supportsTLSv1_3(){
        socket.supportedProtocols.contains("TLSv1.3")
    }
}