package oppen.ariane.io

import org.apache.http.client.utils.URIBuilder
import org.apache.http.client.utils.URIUtilsHC4
import org.junit.Assert
import org.junit.Test
import java.net.URI


class URLTests {

    @Test
    fun url(){
        var uri = URIBuilder()
            .setScheme("gemini")
            .setHost("oppen.digital")
            .build()

        Assert.assertEquals("gemini://oppen.digital", uri.toString())

        uri = URIBuilder()
            .setScheme("gemini")
            .setHost("oppen.digital/")
            .setPath("generative/")
            .build()

        val resolvedA = URIUtilsHC4.resolve(uri, URI.create("../"))

        Assert.assertEquals("gemini://oppen.digital/generative/", uri.toString())
        Assert.assertEquals("gemini://oppen.digital/", resolvedA.toString())

        val resolvedB = URIUtilsHC4.resolve(uri, URI.create("../software/"))
        Assert.assertEquals("gemini://oppen.digital/software/", resolvedB.toString())

        uri = URIBuilder()
            .setScheme("gemini")
            .setHost("oppen.digital/hello/world/")
            .build()

        val resolvedC = URIUtilsHC4.resolve(uri, URI.create("/"))
        Assert.assertEquals("gemini://oppen.digital/", resolvedC.toString())

        val resolvedD = URIUtilsHC4.resolve(uri, URI.create("/foo/"))
        Assert.assertEquals("gemini://oppen.digital/foo/", resolvedD.toString())
    }
}