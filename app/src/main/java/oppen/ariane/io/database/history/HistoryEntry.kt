package oppen.ariane.io.database.history

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.net.Uri

class HistoryEntry(
    val uid: Int,
    val timestamp: Long,
    val uri: Uri
)