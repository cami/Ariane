package oppen.ariane.io.database.bookmarks

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import java.net.URI

class BookmarkEntry(
    val uid: Int,
    val label: String,
    val uri: URI,
    val index: Int
){
    var visible = true
}