package oppen.ariane.io.database

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.content.Context
import androidx.room.Room
import oppen.ariane.io.database.bookmarks.ArianeBookmarks
import oppen.ariane.io.database.history.ArianeHistory

class ArianeDatabase(context: Context) {

    private val db: ArianeAbstractDatabase = Room.databaseBuilder(context, ArianeAbstractDatabase::class.java, "ariane_database_v1").build()

    fun bookmarks(): ArianeBookmarks = ArianeBookmarks(db)
    fun history(): ArianeHistory = ArianeHistory(db)
}